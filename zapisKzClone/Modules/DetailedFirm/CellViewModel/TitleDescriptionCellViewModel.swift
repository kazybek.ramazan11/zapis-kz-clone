//
//  TitleDescriptionCellViewModel.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/21/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

class TitleDescriptionCellViewModel: DetailedFirmCellViewModel {
    var type: DetailedFirmCellType = .titleWithDescription
    
    let title, description: String
    
    init(title: String, description: String) {
        self.title = title
        self.description = description
    }
}
