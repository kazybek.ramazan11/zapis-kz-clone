//
//  DetailedFirmCellViewModel.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/21/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

protocol DetailedFirmCellViewModel {
    var type: DetailedFirmCellType { get set }
}
