//
//  DetailedFirmCellType.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/21/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

enum DetailedFirmCellType: String {
    case titleWithDescription
    case contactInfo
    case assessment
    case workTime
    case masterInfo
    case services
    case categories
    case location
    
    var identifier: String {
        switch self {
        case .titleWithDescription:
            return TitleDescriptionCell.identifier
        case .contactInfo:
            return ""
        case .assessment:
            return ""
        case .workTime:
            return ""
        case .masterInfo:
            return ""
        case .services:
            return ""
        case .categories:
            return ""
        case .location:
            return ""
        }
    }
}
