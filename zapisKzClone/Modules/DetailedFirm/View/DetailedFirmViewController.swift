//
//  DetailedFirmViewController.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/21/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import UIKit

class DetailedFirmViewController: UIViewController {
    private let viewModel: DetailedFirmViewModel
    
    private lazy var tableView: UITableView = {
        let view = UITableView(frame: .zero)
        view.showsVerticalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.separatorStyle = .none
        view.backgroundColor = Tint.backgroundColor
        view.estimatedRowHeight = 100
        view.tableFooterView = UIView()
        view.dataSource = self
        view.delegate = self

        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupObservers()
        setupViews()
        setupConstraints()
    }
    
    init(detailed firm: DetailedFirm) {
        viewModel = DetailedFirmViewModel(firm: firm)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - UITableViewDelegate
extension DetailedFirmViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.items[0].type.identifier, for: indexPath) as! DetailedFirmConfigurableCell
        cell.configure(with: viewModel.items[indexPath.row])
        
        return cell
    }
}

private extension DetailedFirmViewController {
    //MARK: - ConfigUI
    private func setupViews() {
        tableView.tableHeaderView = ImageCollectionView(images: viewModel.firm.firm.pictures)
    }
    
    private func setupConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    private func setupObservers() {
        viewModel.updateTableView = { [weak self] in
            self?.tableView.reloadData()
        }
    }
}
