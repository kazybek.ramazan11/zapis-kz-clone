//
//  DetailedFirmViewModel.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/21/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

class DetailedFirmViewModel {
    let firm: DetailedFirm
    var registeredCellIDs = Set<String>()
    private let titleDescription: TitleDescriptionCellViewModel
    
    var items = [DetailedFirmCellViewModel]()
    var updateTableView: (() -> Void)?
    
    init(firm: DetailedFirm) {
        self.firm = firm
        
        titleDescription = TitleDescriptionCellViewModel(title: firm.firm.category, description: firm.firm.name)
        
        setupItems()
        registerCells()
    }
    
    func setupItems() {
        items = [
            titleDescription
        ]
    }
    
    func updatePage() {
        updateTableView?()
    }
    
    func registerCells() {
        items.forEach { registeredCellIDs.insert($0.type.identifier) }
    }
}
