//
//  ViewController.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import UIKit
import SnapKit

class MainViewController: UIViewController, Refreshable {
    private let viewModel = MainViewModel()
    
    internal lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(fetchList), for: .valueChanged)
        
        return control
    }()
    
    private lazy var tableView: UITableView = {
        let view = UITableView(frame: .zero, style: .grouped)
        view.showsVerticalScrollIndicator = false
        view.separatorStyle = .none
        view.backgroundColor = Tint.backgroundColor
        view.refreshControl = refreshControl
        view.register(FirmCollectionCell.self, forCellReuseIdentifier: FirmCollectionCell.identifier)
        view.rowHeight = 270
        view.tableFooterView = UIView()
        view.dataSource = self
        view.delegate = self
        
        return view
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        navigationController?.navigationBar.shadowImage = UIImage()
//        navigationController?.navigationBar.isTranslucent = true
        
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupObservers()
        setupViews()
        fetchList()
    }

}

//MARK: - UITableViewDelegate
extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FirmCollectionCell.identifier, for: indexPath) as! FirmCollectionCell
        cell.list = viewModel.getList(for: indexPath.section)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.titleForHeader(in: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
}


private extension MainViewController {
    //MARK: - Requests
    @objc func fetchList() {
        startRefreshing()
        viewModel.getHomeElements()
    }
    
    //MARK: - Methods
    func setupObservers() {
        viewModel.$list.observe(self, closure: { (vc, list) in
            if vc.refreshControl.isRefreshing {
                vc.stopRefreshing()
            }
            vc.tableView.reloadData()
        })
    }

    
    //MARK: - ConfigUI
    func setupViews() {
        
        view.addSubview(tableView)
        
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}
