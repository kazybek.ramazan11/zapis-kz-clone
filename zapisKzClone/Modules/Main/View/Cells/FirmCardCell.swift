//
//  FirmCardCell.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import UIKit

class FirmCardCell: UICollectionViewCell {
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var cellContentView: UIView!
    @IBOutlet weak var firmImageView: UIImageView!
    @IBOutlet weak var numberUsedButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        cellContentView.layer.cornerRadius = cellContentView.frame.width / 10
    }
    
    func setFirm(firm: Firm) {
        categoryNameLabel.text = firm.type
        titleLabel.text = firm.name
        
        firm.address.attributedStringFromHTML { [weak self] text in
            self?.addressLabel.text = text?.string
        }
//        if let urlString = firm.pictureUrl, let url = URL(string: urlString) {
//            firmImageView.kf.setImage(with: url)
//        }
        firmImageView.image = #imageLiteral(resourceName: "other_docs")
        firmImageView.contentMode = .scaleAspectFit
        if firm.checkRating > 0 {
            numberUsedButton.setTitle(firm.checkRating.description, for: .normal)
            numberUsedButton.setImage(#imageLiteral(resourceName: "subscribers"), for: .normal)
        }else {
            numberUsedButton.setImage(nil, for: .normal)
            numberUsedButton.setTitle(nil, for: .normal)
        }
    }
}
