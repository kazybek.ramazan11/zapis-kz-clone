//
//  FirmCollectionCell.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import UIKit
protocol FirmCollectionCellDelegate: class {
    func didSelect(firm: Firm)
}

class FirmCollectionCell: UITableViewCell {
    weak var delegate: FirmCollectionCellDelegate?
    
    var list = [Firm]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    struct Constants {
        static let margin = 12
        static let numberOfColumns = 2
        static let contentInset = 12
    }
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let width = (CGFloat(self.frame.width  * 0.6))
        let height = CGFloat(width) * 1.34
        layout.itemSize = CGSize(width: width, height: height)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 12
        layout.minimumLineSpacing = 12

        layout.sectionHeadersPinToVisibleBounds = false
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .clear
        view.alwaysBounceHorizontal = true
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        
        view.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        view.register(UINib(nibName: FirmCardCell.identifier, bundle: nil), forCellWithReuseIdentifier: FirmCardCell.identifier)
        view.delegate = self
        view.dataSource = self

        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - ConfigUI
    private func setupViews() {
        backgroundColor = .clear
        
        addSubview(collectionView)
        
        collectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

//MARK: - UICollectionViewDelegate
extension FirmCollectionCell: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FirmCardCell.identifier, for: indexPath) as! FirmCardCell
        cell.setFirm(firm: list[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelect(firm: list[indexPath.row])
    }
}
