//
//  Home.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

struct Home: Codable {
    let recommendedFirms: [Firm]
    let popularFirms: [Firm]
    let recentlyAddedFirms: [Firm]
    let masters: [Firm]
    
    let isCategoriesHidden: Bool
}
