//
//  DetailedFirm.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/21/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

struct DetailedFirm: Decodable {
    let firm: Firm
    let masters: [Master]
    let services: [Services]
    let isOnline: Bool
    let location: Location
    let categories: [Categories]
    struct Categories: Decodable {
        let id: Int
        let name: String
    }
    
    struct Firm: Decodable {
        let id: Int
        let category: String
        let name: String
        let address: String
        let workStartTime, workEndTime: String
        let type: String?
        let checkRating: Int
        let urlKey: String?
        let instagramProfile: String?
        let isPromoted: Bool?
        let avatarUrl: String?
        let cityName: String
        let isMastersHidden: Bool
        let phoneNumbers: [String]?
        let pictures: [String]
    }
}

struct Master: Decodable {
    let id: Int
    let name, surname: String
    let profession: String
    let serviceIds: [Int]
    let avatarUrl: String
    let rating: Int
    let experience: String
    let isRoom: Bool
}

struct Services: Decodable {
    let id: Int
    let name: String
    let price: Int
    let duration: Int
    let priceStr: String
    let categoryId: Int
}

struct Location: Decodable {
    let type: String
    let markerX, markerY, centerX, centerY: Double
    let zoom: Int
}


//{
//    "message": "",
//    "data": {
//        "firm": {
//            "id": 134,
//            "category": "SALON",
//            "name": "Divine",
//            "address": "пр. Абая 20/1",
//            "workStartTime": "21-06-2020 10:00",
//            "workEndTime": "21-06-2020 20:00",
//            "description": "",
//            "type": "Салон красоты",
//            "checkRating": 0,
//            "urlKey": "divine-134",
//            "instagramProfile": "salon_divine_almaty",
//            "isClientSurnameRequired": false,
//            "avatarUrl": "/static/img/picture-missing/salon_avatar.jpg?v=1",
//            "isExpress": false,
//            "phoneNumbers": [
//                "+7(707)635-78-03"
//            ],
//            "pictures": [
//                "/static/img/picture-missing/salon_slider.jpg?v=1",
//                "/static/img/picture-missing/salon_slider.jpg?v=1",
//                "/static/img/picture-missing/salon_slider.jpg?v=1",
//                "/static/img/picture-missing/salon_slider.jpg?v=1"
//            ],
//            "isPromoted": false,
//            "isMastersHidden": false,
//            "cityName": "Алматы",
//            "todayReservationsCount": null
//        }
//        "categories": [
//            {
//                "id": -2,
//                "name": "Акция"
//            },
//            {
//                "id": -1,
//                "name": "Популярные"
//            },
//            {
//                "id": 1,
//                "name": "Волосы"
//            },
//            {
//                "id": 2,
//                "name": "Ногти"
//            },
//            {
//                "id": 5,
//                "name": "Эпиляция"
//            },
//            {
//                "id": 13,
//                "name": "Брови"
//            }
//        ],
//        "location": {
//            "type": "TWO_GIS",
//            "markerX": 76.94323,
//            "markerY": 43.239887,
//            "centerX": 76.94336,
//            "centerY": 43.239872,
//            "zoom": 17
//        },
//        "instagram": null
//    }
//}
