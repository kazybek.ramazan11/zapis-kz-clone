//
//  Firm.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

struct Firm: Codable {
    let id: Int
    let name: String
    let address: String
    let type: String?
    let checkRating: Int
    let urlKey: String?
    let isPromoted: Bool?
    let avatarUrl: String?
    let isIndividualMaster: Bool
    let workSchedule: String?
    let pictureUrl: String?
    let averageRating: Float
    let isFavorite: Bool?
}

//"id": 579,
//"name": "Nail bar Almaty",
//"address": "ул. Ади Шарипова 103\\2 уг. ул. Карасай батыра",
//"type": "Салон красоты",
//"checkRating": 2,
//"urlKey": "nail-bar-almaty-579",
//"isPromoted": false,
//"avatarUrl": "/static/img/picture-missing/salon_avatar.jpg?v=1",
//"isIndividualMaster": false,
//"workSchedule": "10:00 - 20:00",
//"pictureUrl": "/static/img/picture-missing/salon_slider.jpg?v=1",
//"averageRating": 4.8,
//"todayReservationsCount": null,
//"isFavorite": false
