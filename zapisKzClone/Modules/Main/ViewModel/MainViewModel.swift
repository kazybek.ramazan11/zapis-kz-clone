//
//  MainViewModel.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation
import PromiseKit

enum FirmType: Int {
    case recommendedFirms = 0
    case popularFirms
    case masters
    case recentlyAddedFirms
}

class MainViewModel {
    private let networkManager = MainNetworkManager()
    @Observable var list: Home?
    
    func getHomeElements() {
        firstly {
            networkManager.getHomeElements()
        }.done { [weak self] result in
            self?.list = result
        }.catch { error in
            
        }
    }
    
    func getList(for row: Int) -> [Firm] {
        guard let list = list else { return [] }
        
        switch FirmType(rawValue: row) {
        case .recommendedFirms:
            return list.recommendedFirms
        case .popularFirms:
            return list.popularFirms
        case .masters:
            return list.masters
        case .recentlyAddedFirms:
            return list.recentlyAddedFirms
        case .none:
            return []
        }
    }
    
    func titleForHeader(in section: Int) -> String? {
        switch FirmType(rawValue: section) {
        case .recommendedFirms:
            return "Рекомендуемые"
        case .popularFirms:
            return "Популярные"
        case .masters:
            return "Индивидуальные мастера"
        case .recentlyAddedFirms:
            return "Недавно добавленные"
        case .none:
            return nil
        }
    }
}
