//
//  MainNetworkManager.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation
import PromiseKit

class MainNetworkManager {
    private let networkClient: NetworkClient
    private let requestProvider = MainRequestProvider()

    init() {
        networkClient = AlamofireNetworkClient()
    }
}

extension MainNetworkManager {
    func getHomeElements() -> Promise<Home> {
        let data = requestProvider.makeRequest(for: .home)
        return Promise { seal in
            networkClient.performRequest(data, type: Home.self).done { result in
                seal.fulfill(result)
            }.catch { error in
                seal.reject(error)
            }
        }
    }
}
