//
//  MainRequestProvider.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

class MainRequestProvider: BaseRequestProvider {
    func makeRequest(for path: MainPath) -> RequestData {
        switch path {
        case .home:
            return makeRequest(from: path)
        }
    }
    
    private func makeRequest(from path: MainPath) -> RequestData {
        let requestData = RequestData(path:  Constant.API.baseAPIURL + path.url,
                                      method: path.method,
                                      query:  path.parameters)
        return makeRequest(request: requestData)
    }
}
