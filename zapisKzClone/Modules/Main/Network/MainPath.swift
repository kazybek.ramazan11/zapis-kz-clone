//
//  MainRequestProvider.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation
import Alamofire

enum MainPath: APIRouter {
    case home
    
    var url: String {
        switch self {
        case .home:
            return "screen/home"
        }
    }
    
    var method: Method {
        switch self {
        default:
            return .get
        }
    }
    
    var parameters: QueryParameters? {
        switch self {
        default:
            return nil
        }
    }
}
