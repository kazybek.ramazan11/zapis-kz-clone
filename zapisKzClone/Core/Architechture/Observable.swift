//
//  Observable.swift
//  Implementation inspired by
//  https://www.swiftbysundell.com/posts/observers-in-swift-part-2
//
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

@propertyWrapper
public class Observable<Value> {
    private var observations = [UUID: (Value?) -> Void]()

    public init(wrappedValue: Value?) {
        self.wrappedValue = wrappedValue
    }

    public var wrappedValue: Value? {
        didSet {
            valueDidChanged()
        }
    }

    public var projectedValue: Observable<Value> {
        return self
    }

    private func valueDidChanged() {
        observations.values.forEach { closure in
            closure(wrappedValue)
        }
    }

    @discardableResult
    public func observe<O: AnyObject>(_ observer: O, closure: @escaping (O, Value?) -> Void) -> ObservationToken {
        let id = UUID()

        observations[id] = { [weak self, weak observer] item in
            // If the observer has been deallocated, we can
            // automatically remove the observation closure.
            guard let observer = observer else {
                self?.removeObserver(for: id)
                return
            }

            closure(observer, item)
        }

        if let value = wrappedValue {
            closure(observer, value)
        }

        return ObservationToken { [weak self] in
            self?.removeObserver(for: id)
        }
    }

    private func removeObserver(for id: UUID) {
        observations.removeValue(forKey: id)
    }
}

public class ObservationToken {
    private let cancellationClosure: () -> Void

    public init(cancellationClosure: @escaping () -> Void) {
        self.cancellationClosure = cancellationClosure
    }

    public func cancel() {
        cancellationClosure()
    }
}

private extension Dictionary where Key == UUID {
    mutating func insert(_ value: Value) -> UUID {
        let id = UUID()
        self[id] = value
        return id
    }
}
