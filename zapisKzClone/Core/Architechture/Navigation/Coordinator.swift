//
//  Coordinator.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import UIKit

public class Coordinator {
    public static let `default` = Coordinator()
    
    public func presentMain() {
        UIApplication.shared.windows.first?.rootViewController = MainViewController()
    }
}
