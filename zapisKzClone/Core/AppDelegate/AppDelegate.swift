//
//  AppDelegate.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let vc = MainViewController()
        let navigation = UINavigationController(rootViewController: vc)
        window = UIWindow(frame: UIScreen.main.bounds)

        window?.rootViewController = navigation
        window?.makeKeyAndVisible()
        return true
    }


}

