//
//  ThirdPartiesAppDelegateConfigurator.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import UIKit
import SVProgressHUD
import Kingfisher

class ThirdPartiesAppDelegateConfigurator: AppDelegateConfigurator {
    private func configureKingfisher() {
        ImageCache.default.diskStorage.config.sizeLimit = 1000 * 1024 * 1024
        ImageCache.default.cleanExpiredDiskCache()
    }

    private func configureSVProgressHUD() {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultMaskType(.clear)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configureKingfisher()
        configureSVProgressHUD()
        
        return true
    }
}
