//
//  AppDelegateConfiguratorFactory.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

enum AppDelegateConfiguratorFactory {
    static func makeDefault() -> AppDelegateConfigurator {
        let configuratos: [AppDelegateConfigurator] = [
            StartupAppDelegateConfigurator()
        ]
        
        return CompositeAppDelegateConfigurator(configurators: configuratos)
    }
}
