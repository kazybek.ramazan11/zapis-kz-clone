//
//  CompositeAppDelegateConfigurator.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import UIKit

typealias AppDelegateConfigurator = UIResponder & UIApplicationDelegate

class CompositeAppDelegateConfigurator: AppDelegateConfigurator {
    private let configurators: [AppDelegateConfigurator]
    
    init(configurators: [AppDelegateConfigurator]) {
        self.configurators = configurators
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        configurators.forEach { _ = $0.application?(application, didFinishLaunchingWithOptions: launchOptions) }
        
        return true
    }
}
