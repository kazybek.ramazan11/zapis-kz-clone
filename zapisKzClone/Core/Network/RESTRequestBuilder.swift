//
//  RESTRequestBuilder.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

class RESTRequestBuilder {
    func buildURL(path: String, queries: QueryParameters?) -> URL {
        guard let url = URL(string: path) else {
            fatalError("Provided url does not exist")
        }
        
        if let queryParameters = queries,
            var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) {
            
            urlComponents.queryItems = queryParameters.compactMap({ (value) -> URLQueryItem? in
                return URLQueryItem(name: value.key, value: value.value)
            })
            
            guard let url = urlComponents.url else { fatalError() }
            
            return url
        }
        
        return url
    }
}
