//
//  NetworkClient.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation
import PromiseKit

protocol NetworkClient {
    func performRequest<T: Codable>(_ request: RequestData, type: T.Type) -> Promise<T>
}

enum Method: String {
    case post = "POST"
    case get = "GET"
    case put = "PUT"
}

typealias Headers = [String: String]
typealias BodyParameters = [String: Any]
typealias QueryParameters = [String: String]
