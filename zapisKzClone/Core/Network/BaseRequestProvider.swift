//
//  BaseRequestProvider.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

class BaseRequestProvider {
    private func makeBaseHeaders() -> Headers {
        return [
            "ContentType": "application/json"
        ]
    }
    
    func makeRequest(request: RequestData) -> RequestData {
        var headers: Headers = [:]
        
        makeBaseHeaders().forEach { headers[$0.key] = $0.value }
        request.headers?.forEach { headers[$0.key] = $0.value }
        
        
        return RequestData(path: request.path, method: request.method, headers: headers, parameters: request.parameters, query: request.query, encoding: request.encoding)
    }
    
    
}
