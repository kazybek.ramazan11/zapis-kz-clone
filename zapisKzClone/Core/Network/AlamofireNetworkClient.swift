//
//  AlamofireNetworkClient.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Alamofire
import PromiseKit

struct ServerResponse<T: Codable>: Codable {
    let data: T?
    let message: String?
}

class AlamofireNetworkClient: NetworkClient {
    private let requestBuilder = RESTRequestBuilder()
    
    var currentTask: DataRequest?
    
    let defaultManager: SessionManager = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "http://zp.jgroup.kz": .disableEvaluation
        ]
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        
        return SessionManager(configuration: configuration,
                              serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
    }()
    
    func performRequest<T: Codable>(_ request: RequestData, type: T.Type) -> Promise<T> {
        return Promise<T> { seal in
            let url = requestBuilder.buildURL(path: request.path, queries: request.query)
            let task = defaultManager.request(url,
                                              method: request.httpMethod,
                                              parameters: request.parameters,
                                              encoding: request.encoding,
                                              headers: request.headers)
            currentTask = task
            
            task.responseJSON { [weak self] response in
                switch response.response?.statusCode {
                case 204:
                    seal.reject(AsyncError.noResponse)
                default: break
                }
                
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        seal.reject(AsyncError.unexpected)
                        return
                    }
                    
                    do {
                        let decoder = JSONDecoder()
                        decoder.dateDecodingStrategy = .millisecondsSince1970
                        let result = try decoder.decode(ServerResponse<T>.self, from: data)
                        guard let data = result.data else {
                            seal.reject(AsyncError.unexpected)
                            return
                        }
                        seal.fulfill(data)
                    }catch {
                        Log.error("Parse failed")
                        seal.reject(AsyncError.unexpected)
                    }
                case .failure:
                    Log.error("Parse failed")
                    seal.reject(AsyncError.unexpected)
                }
                self?.currentTask = nil
            }
        }
    }
}

private extension RequestData {
    var httpMethod: HTTPMethod {
        switch method {
        case .get:
            return .get
        case .post:
            return .post
        case .put:
            return .put
        }
    }
}
