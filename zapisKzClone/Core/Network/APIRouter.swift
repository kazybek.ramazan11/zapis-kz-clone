//
//  APIRouter.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation
import Alamofire

protocol APIRouter {
    var url: String { get }
    var method: Method { get }
    var parameters: QueryParameters? { get }
}
