//
//  RequestData.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation
import Alamofire

struct RequestData {
    var path: String
    var method: Method
    var headers: Headers?
    var parameters: BodyParameters?
    var query: QueryParameters?
    var encoding: ParameterEncoding
    
    init(path: String,
         method: Method = .get,
         headers: Headers? = ["Content-Type": "application/json"],
         parameters: Parameters? = nil,
         query: QueryParameters? = nil,
         encoding: ParameterEncoding? = nil) {
        
        self.path = path
        self.method = method
        self.headers = headers
        self.parameters = parameters
        self.query = query
        self.encoding = (method == .get) ? URLEncoding.default : JSONEncoding.default
        if let encoding = encoding {
            self.encoding = encoding
        }
    }
}

