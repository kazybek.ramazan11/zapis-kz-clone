//
//  AsyncError.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

typealias Callback = () -> Void
typealias LogicalCallback = () -> Bool


struct AsyncError: Error {
    enum ErrorType {
        case system
        case validation([String])
    }
    
    let code: String?
//    LocalizedText
    let title: String
    let description: String?
    let type: ErrorType
    
    init(code: String? = nil, title: String, description: String? = nil, errorType: ErrorType = .system) {
        self.code = code
        self.title = title
        self.description = description
        self.type = errorType
    }
}

extension AsyncError {
    static var unexpected: AsyncError {
        return AsyncError(title: "Пожалуйста, повторите позже")
    }
    
    static var noResponse: AsyncError {
        return AsyncError(title: "No Response")
    }
}
