//
//  ImageCollectionCell.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/21/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import UIKit

class ImageCollectionView: UIView {
    private var currentPage: Int = 1
    private var pageControl = UIPageControl()
    
    var images = [String]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: self.frame.width, height: self.frame.height)
        layout.scrollDirection = .horizontal

        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .clear
        view.alwaysBounceHorizontal = true
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.isPagingEnabled = true
        view.register(UICollectionViewCell.self, forCellWithReuseIdentifier: UICollectionViewCell.identifier)
        view.dataSource = self
        
        return view
    }()
    
    init(images: [String]) {
        self.images = images
        
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func generateImageView(for url: String) -> UIImageView? {
        let imageView = UIImageView()
        guard let url = URL(string: "\(Constant.API.baseURL)\(url)") else { return nil}
        imageView.kf.setImage(with: url)
        
        return imageView
    }
    
    private func configurePageControl() {
        self.pageControl.numberOfPages = 3
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = .white
        self.pageControl.pageIndicatorTintColor = .lightGray
        self.pageControl.currentPageIndicatorTintColor = Tint.paletteVioletColor
    }
}

extension ImageCollectionView: UICollectionViewDataSource, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UICollectionViewCell.identifier, for: indexPath)
        if let imageView = generateImageView(for: images[indexPath.row]) {
            cell.addSubview(imageView)
        }
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.width
        currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        pageControl.currentPage = currentPage
    }
}
