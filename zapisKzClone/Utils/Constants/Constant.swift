//
//  Constant.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

struct Constant {
    struct API {
        static let baseAPIURL = "http://zp.jgroup.kz/rest/clients-app/v1/"
        static let baseURL = "http://zp.jgroup.kz"
    }
}
