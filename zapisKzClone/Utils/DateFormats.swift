//
//  DateFormats.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

struct FormatUtil {
    
    /// Format Ex: "HH:mm:ss"
    static var log: DateFormatter = {
        let logFormatter = DateFormatter()
        logFormatter.locale = Locale(identifier: "en-US")
        logFormatter.dateFormat = "HH:mm:ss"
        return logFormatter
    }()
}
