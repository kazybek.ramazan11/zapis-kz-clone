//
//  Log.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

class Log {
    
    // MARK: - Computed properties
    
    private static var date: String {
        return FormatUtil.log.string(from: Date())
    }
    
    private static var thread: String {
        if Thread.isMainThread {
            return "main"
        } else if let threadName = Thread.current.name, !threadName.isEmpty {
            return threadName
        } else if let queueName = String(validatingUTF8: __dispatch_queue_get_label(nil)), !queueName.isEmpty {
            return queueName
        } else {
            return String(format: "%p", Thread.current)
        }
    }
    
    // MARK: - Enum defining our log levels
    
    private enum Level: CustomStringConvertible {
        case info
        case warning
        case error
        
        var description: String {
            switch self {
            case .info:
                return "Info 📝"
            case .warning:
                return "Warning ⚠️"
            case .error:
                return "Error 🆘❗"
            }
        }
    }
    
    // MARK: - Private methods
    
    private static func path(from file: StaticString, with line: Int) -> String {
        let file = NSString(string: String(describing: file)).lastPathComponent
        let line = String(line)
        return "\(file):\(line)"
    }
    
    private static func write(level: Level, file: StaticString, line: Int, message: String, error: Error? = nil) {
        #if !RELEASE
        var output: String = ""
        output += "\(date) "
        output += "[\(level)] "
        output += "[\(path(from: file, with: line))] "
        output += "[\(thread)] "
        output += "> \(message)"
        
        if let error = error {
            print("\(output) > \(error)")
        } else {
            print(output)
        }
        #endif
    }
    
    // MARK: - Public methods
    
    static func info(_ message: String, file: StaticString = #file, line: Int = #line) {
        write(level: .info, file: file, line: line, message: message)
    }
    
    static func warn(_ message: String, file: StaticString = #file, line: Int = #line) {
        write(level: .warning, file: file, line: line, message: message)
    }
    
    static func error(_ message: String, file: StaticString = #file, line: Int = #line, error: Error? = nil) {
        write(level: .error, file: file, line: line, message: message, error: error)
    }
    
}
