//
//  AsyncResult.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation

enum AsyncResult<T> {
    case success(data: T)
    case failure(error: AsyncError)
}

extension AsyncResult where T == Void {
    static var success: AsyncResult {
        return .success(data: ())
    }
}
