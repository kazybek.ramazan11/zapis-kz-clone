//
//  UICollectionView+Identifier.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import UIKit

public extension UICollectionViewCell {
    class var identifier: String {
        let result = String(describing: self)
        return result
    }
}
