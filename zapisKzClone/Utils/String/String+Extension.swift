//
//  String+Extension.swift
//  zapisKzClone
//
//  Created by Ramazan Kazybek on 6/20/20.
//  Copyright © 2020 Ramazan Kazybek. All rights reserved.
//

import Foundation
extension String {
    func attributedStringFromHTML(defaultAttributes: [NSAttributedString.Key : Any] = [:], completionBlock: @escaping (NSAttributedString?) ->()) {
        guard let data = self.data(using: .utf8) else {
            print("Unable to decode data from html string: \(self)")
            return completionBlock(nil)
        }
        
        let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
            NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.html,
            NSAttributedString.DocumentReadingOptionKey.characterEncoding: NSNumber(value:String.Encoding.utf8.rawValue)
        ]
        
        DispatchQueue.main.async {
            if let attributedString = try? NSMutableAttributedString(data: data, options: options, documentAttributes: nil) {
                
                attributedString.addAttributes(defaultAttributes, range: NSRange(location: 0, length: attributedString.length))
                
                completionBlock(attributedString)
            } else {
                print("Unable to create attributed string from html string: \(self)")
                completionBlock(nil)
            }
        }
    }
}
